#!/usr/bin/env sh
# generates signature key
if [ "$#" -lt 1 ]; then
  echo 'usage: ' $0 '<keystore_path>'
  exit 1
fi

rm $1
keytool -genkey -alias valerino -keyalg RSA -sigalg SHA1withRSA -keypass valerino -keystore $1 -storepass valerino -validity 9999

