#!/usr/bin/env sh
# extracts the apk to a temporary folder and print the certificate information

if [ "$#" -ne 1 ]; then
  echo 'usage:' $0 '<path/to/apk>'
  exit 1
fi

# extract here
_EXDIR=/tmp/signtmp

# extract to tmp 
rm -rf $_EXDIR
mkdir -p $_EXDIR
unzip $1 -d $_EXDIR > /dev/null

# print any DSA signature
for f in $_EXDIR/META-INF/*.DSA; do
    [ -e "$f" ] && keytool -printcert -file $f
done

for f in $_EXDIR/META-INF/*.RSA; do
    [ -e "$f" ] && keytool -printcert -file $f
done

