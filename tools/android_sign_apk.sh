#!/usr/bin/env sh
if [ "$#" -ne 2 ]; then
  echo usage: $0 '<path/to/key/generated/with/android_gen_key.sh> <path/to/apk>'
  exit 1
fi

# resign apk
jarsigner -verbose -keystore $1 -keypass valerino -storepass valerino $2 valerino



