#!/usr/bin/env sh
# starts adb and grep for the given string case-insensitive (without using adb filtering, which sucks!)
if [ "$#" -ne 1 ]; then
  echo "usage: $0 <to_grep>" 
  exit 1
fi

adb logcat -c
adb logcat | grep -i $1

