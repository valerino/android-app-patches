#!/usr/bin/env sh
# usage: rebuild_and_upload <path/to/decompiled/app

# uninstall existing, if any
echo "[.] uninstall existing app"
rm ./com.whatsapp.apk
adb uninstall com.whatsapp

# recompile
echo "[.] recompile $1 to ./com.whatsapp.apk"
../../apktool.sh b $1 -o ./com.whatsapp.apk        

# resign
echo "[.] resign ./com.whatsapp.apk"
../../tools/android_sign_apk.sh ./fake_wa.key ./com.whatsapp.apk

# make backup and install
echo "[.] reinstall to device"
cp ./com.whatsapp.apk ./com.whatsapp.cracked.apk
adb install ./com.whatsapp.apk

