# whatsapp sanity check patches

valerino, cy4gate, y2k20

tested with: [WhatsApp 2.20.27](https://www.apkmirror.com/apk/whatsapp-inc/whatsapp/whatsapp-2-20-27-release/whatsapp-messenger-2-20-27-3-android-apk-download/download/)

decompiled/recompiled with: [Apktool 2.4.1](https://bitbucket.org/iBotPeaches/apktool/downloads/apktool_2.4.1.jar)


## step 1: decompile

~~~bash
// decompile to 'decompiled' folder
apktool d ./com.whatsapp.original.apk -f -o ./decompiled
~~~

# step 2:
copy [Patches.smali](./Patches.smali) to ./decompiled/smali/X
~~~bash
cp ./Patches.smali ./decompiled/smali/X/Patches.smali
~~~

## step 3: search for package signature checks

~~~
// search for access to signatures object
grep -iRn ";->signatures" ./decompiled
./decompiled/smali/X/0NW.smali:132:    iget-object p0, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;
./decompiled/smali/X/0NW.smali:208:    iget-object v0, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;
./decompiled/smali/X/0UA.smali:91:    iget-object v1, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;
./decompiled/smali/X/0UA.smali:158:    iget-object v1, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;
./decompiled/smali/X/2gD.1.smali:221:    iget-object v0, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;
./decompiled/smali/X/062.smali:118:    iget-object v4, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;
./decompiled/smali/X/00O.smali:1717:    iget-object v0, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;
./decompiled/smali/X/00O.smali:1736:    iget-object v1, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;
./decompiled/smali/X/0NV.smali:395:    iget-object v0, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;
./decompiled/smali/X/0NV.smali:399:    iget-object v0, v9, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;
./decompiled/smali/X/01Y.smali:10027:    iget-object v2, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;
./decompiled/smali/com/google/android/search/verification/client/SearchActionVerificationClientUtil.smali:174:    iget-object v1, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;
~~~

### patch package signature checks

__every__ call to:

~~~bash
iget-object dst, src, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;
~~~

must be __followed__ by:

~~~bash
invoke-static {}, LX/Patches;->originalSignature()[Landroid/content/pm/Signature;
move-result-object dst
~~~

__take particular care in this part__, which involves Google Play authentication:

~~~bash
    iget-object v0, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    aget-object v1, v0, v7

    iget-object v0, v9, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    aget-object v0, v0, v7

    .line 44721
    invoke-virtual {v1, v0}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z
~~~

which becomes like:

~~~bash
    iget-object v0, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    invoke-static {}, LX/Patches;->originalSignature()[Landroid/content/pm/Signature;

    move-result-object v0

    aget-object v1, v0, v7

    invoke-static {}, LX/Patches;->originalSignature()[Landroid/content/pm/Signature;

    move-result-object v0

    aget-object v0, v0, v7

    .line 44721
    invoke-virtual {v1, v0}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z
~~~

## step 4: search for classes.dex md5 check

this is found in 2 files, the good one is the one calling __getPackageCodePath()__ right before declaring the _classes.dex_ string.

~~~bash
grep -iRnB10 "const-string v0, \"classes.dex\"" ./decompiled

./decompiled/smali/X/1Qt.1.smali-2472-
./decompiled/smali/X/1Qt.1.smali-2473-    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getPackageCodePath()Ljava/lang/String;
./decompiled/smali/X/1Qt.1.smali-2474-
./decompiled/smali/X/1Qt.1.smali-2475-    move-result-object v0
./decompiled/smali/X/1Qt.1.smali-2476-
./decompiled/smali/X/1Qt.1.smali-2477-    invoke-direct {v5, v0}, Ljava/util/zip/ZipFile;-><init>(Ljava/lang/String;)V
./decompiled/smali/X/1Qt.1.smali-2478-    :try_end_0
./decompiled/smali/X/1Qt.1.smali-2479-    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
./decompiled/smali/X/1Qt.1.smali-2480-
./decompiled/smali/X/1Qt.1.smali-2481-    :try_start_1
./decompiled/smali/X/1Qt.1.smali:2482:    const-string v0, "classes.dex"
--
./decompiled/smali/X/09C.smali-979-    invoke-direct {v1, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
./decompiled/smali/X/09C.smali-980-
./decompiled/smali/X/09C.smali-981-    invoke-direct {v14, v1}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V
./decompiled/smali/X/09C.smali-982-    :try_end_1
./decompiled/smali/X/09C.smali-983-    .catchall {:try_start_1 .. :try_end_1} :catchall_1
./decompiled/smali/X/09C.smali-984-
./decompiled/smali/X/09C.smali-985-    .line 24178
./decompiled/smali/X/09C.smali-986-    :try_start_2
./decompiled/smali/X/09C.smali-987-    new-instance v15, Ljava/util/zip/ZipEntry;
./decompiled/smali/X/09C.smali-988-
./decompiled/smali/X/09C.smali:989:    const-string v0, "classes.dex"
~~~

scroll up to the method declaration (in this case __A0H__):

~~~bash
.method public static A0H(Landroid/content/Context;)[B
    .locals 6

    .line 161120
    :try_start_0
    new-instance v5, Ljava/util/zip/ZipFile;

    new-instance v0, Landroid/content/ContextWrapper;

    invoke-direct {v0, p0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getPackageCodePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/zip/ZipFile;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v0, "classes.dex"
	
	...
~~~

### patch the method to return the correct MD5 as a byte[]

the above whole method body must be replaced with code returning the __MD5 hash of the original classes.dex inside the original WhatsApp APK, the same version as the one being decompiled/recompiled__.

So, let's calculate the md5 using the [provided tool](./calculate_md5_classes_dex.sh):

~~~bash
// this MUST be the ORIGINAL UNTOUCHED APK 
./calculate_md5_classes_dex ./com.whatsapp.original.apk

[....]

[.] calculating md5 of classes.dex
99da039b23504a9471160ae0541ca329  ./classes.dex
~~~

that must be __turned to hex__ (99 da 03 9b 23 50 4a 94 71 16 0a e0 54 1c a3 29) and finally __the hex to base64__, resulting in __mdoDmyNQSpRxFgrgVByjKQ==__

In the end, replace the above method body with the following code.

__remember that method name may/will change across versions!__

~~~bash
.method public static A0H(Landroid/content/Context;)[B
    .locals 1

    const-string p0, "mdoDmyNQSpRxFgrgVByjKQ=="

    const/4 v0, 0x0

    .line 38
    invoke-static {p0, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object p0

    return-object p0
.end method
~~~

## step 5: recompile!

recompile and install, then check if:

* you can receive the verification SMS correctly
* __important__: try to send some messages, call, etc...
  * if this results in a ban, other checks may be patched....
  
~~~bash
// recompile and install using the provided tool (assumes decompiled app in ./decompiled)
./rebuild_and_upload.sh ./decompiled
~~~

