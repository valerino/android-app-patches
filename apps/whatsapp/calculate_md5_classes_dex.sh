#!/usr/bin/env sh
# usage: calculate_md5_classes_dex <path/to/ORIGINAL_APK

# extract original apk
echo "[.] extracting $1 to temp folder..."
unzip $1 -d ./_tmp > /dev/null 
	
# get md5sum of classes dex
echo "[.] calculating md5 of classes.dex"
md5sum ./_tmp/classes.dex
rm -rf ./_tmp
