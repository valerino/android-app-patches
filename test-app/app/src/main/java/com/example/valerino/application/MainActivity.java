package com.example.valerino.application;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.content.pm.Signature;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Context ctx = getApplicationContext();
        byte[] b = Patches.originalMd5(ctx);
        byte[] s = Patches.originalSignatureToByteArray();
        Signature[] ss = Patches.originalSignature();
        String cs = Patches.originalSignatureToCharString();
        Log.d("vvv","len: " + cs.length() + " - " + cs);
        PackageInfo pi = null;
        try {
            pi = this.getApplicationContext().getPackageManager().getPackageInfo(this.getPackageName(),0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        boolean bb = Patches.returnTrue(pi);
        int a = 123;
        int g = 456;
        if (!bb) {
            a = 123;
        }
    }

}
