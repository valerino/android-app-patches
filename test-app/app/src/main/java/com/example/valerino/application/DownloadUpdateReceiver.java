/**
 * implements a broadcast receiver which downloads an apk and attempts to install it
 */
package com.example.valerino.application;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.StrictMode;
import android.os.SystemClock;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadUpdateReceiver extends BroadcastReceiver {
    /**
     * shchedule alarm to download/install
     * @param context
     * @param interval in milliseconds
     */
    static void scheduleAlarm(Context context, long interval) {
        // allow no exceptions on android 6+, which blocks referencing files in the app folder
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        // set the alarm
        // the first update download starts immediately, then it's repeated at every interval
        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, DownloadUpdateReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis(),
                interval, alarmIntent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // trigger the update
        DownloadUpdate.run(context, "http://192.168.178.58/telegram.apk", context.getExternalFilesDir(null) + "/upd.apk");
    }
}
