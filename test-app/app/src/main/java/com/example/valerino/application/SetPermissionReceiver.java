package com.example.valerino.application;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.SystemClock;
import android.util.Log;

import java.io.IOException;

public class SetPermissionReceiver extends BroadcastReceiver {

    /**
     * schedule alarm to set world permissions to the app folder
     * @param context
     * @param interval interval in milliseconds
     */
    static void scheduleAlarm(Context context, long interval) {
        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, SetPermissionReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis(),
                interval, alarmIntent);
    }

    /**
     * runs at every interval, set the permissions to 777 on the app folder and its descendents
     * @param context
     */
    protected void method1(Context context) {
        final PackageManager pm = context.getPackageManager();
        final String pkg = context.getPackageName();
        try {
            PackageInfo pinfo = pm.getPackageInfo(pkg, 0);
            try {
                // unprotect application dir
                Runtime.getRuntime().exec("/system/bin/chmod -R 777 " + pinfo.applicationInfo.dataDir);
                //Log.d("xxx", "Setting permissions done!");
            } catch (IOException e) {
                // error
            }
        } catch (PackageManager.NameNotFoundException e) {
            // error
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // trigger permission update
        method1(context);
    }
}
