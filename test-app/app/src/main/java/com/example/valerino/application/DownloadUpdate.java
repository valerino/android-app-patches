package com.example.valerino.application;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * implements asynchronous download/install of an APK (as for android 6+)
 */
public class DownloadUpdate extends AsyncTask<String,Void,Void> {
    private String _url;
    private String _out;
    private Context _ctx;
    static DownloadUpdate _singleton = null;

    DownloadUpdate(Context ctx, String url, String out) {
        _url = url;
        _ctx = ctx;
        _out = out;
    }

    /**
     * perform installation
     */
    private void method2() {
        //Log.i("xxx", "installing update: " + _out);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse("file://" + _out), "application/vnd.android.package-archive");
        _ctx.startActivity(intent);
    }

    /**
     * perform asynchronous download
     */
    private void method1(){

        //Log.i("xxx", "downloading update: " + _out);
        try {
            // setup the connection
            URL url = new URL(_url);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // prepare output file
            File file = new File(_out);
            FileOutputStream fileOutput = new FileOutputStream(file);
            InputStream inputStream = urlConnection.getInputStream();

            // download the stream
            byte[] buffer = new byte[1024];
            int bufferLength = 0;
            while ( (bufferLength = inputStream.read(buffer)) > 0 ) {
                fileOutput.write(buffer, 0, bufferLength);

            }

            // done
            fileOutput.close();

            // install the update!
            method2();
        }
        catch (Exception e) {
            //Log.d("xxx", "error downloading: " + e.getMessage());
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(String... params) {
        method1();
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
    }

    /**
     * triggers the update, called by the BroadcastReceiver
     * @param ctx
     * @param url the download url
     * @param outPath the destination apk path
     */
    public static void run(Context ctx, String url, String outPath){
        // trigger the update
        _singleton = new DownloadUpdate(ctx, url, outPath);
        _singleton.execute("");
    }
}