#!/usr/bin/env python3
"""
telegram APK patches
wolf, ht, 2k18
"""

import libutils.customizeutils
import libutils.apkutils

# globals
PATH_KEYSTORE = './apps/telegram/fake_telegram.key'
LAUNCHER_PARENT = 'org.telegram.ui'
APPNAME = 'Telegram'

def patch_app(args):
	"""
	patch the application
	"""
	args.path_keystore = PATH_KEYSTORE
	args.launch_activity_parent = LAUNCHER_PARENT
	libutils.customizeutils.customize_default_patch(args)

def main():
	libutils.customizeutils.customize_default_main(APPNAME, patch_app)


if __name__ == "__main__":
	main()
