.class public L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;
.super Landroid/os/AsyncTask;
.source "__PLACEHOLDER_CLASS_NAME__.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field static _singleton:L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;


# instance fields
.field private _ctx:Landroid/content/Context;

.field private _out:Ljava/lang/String;

.field private _url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    const/4 v0, 0x0

    sput-object v0, L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;->_singleton:L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "out"    # Ljava/lang/String;

    .line 24
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 25
    iput-object p2, p0, L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;->_url:Ljava/lang/String;

    .line 26
    iput-object p1, p0, L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;->_ctx:Landroid/content/Context;

    .line 27
    iput-object p3, p0, L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;->_out:Ljava/lang/String;

    .line 28
    return-void
.end method

.method private method1()V
    .locals 9

    .line 48
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;->_url:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 49
    .local v0, "url":Ljava/net/URL;
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    check-cast v1, Ljava/net/HttpURLConnection;

    .line 50
    .local v1, "urlConnection":Ljava/net/HttpURLConnection;
    const-string v2, "GET"

    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 51
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V

    .line 54
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;->_out:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 55
    .local v2, "file":Ljava/io/File;
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 56
    .local v3, "fileOutput":Ljava/io/FileOutputStream;
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    .line 59
    .local v4, "inputStream":Ljava/io/InputStream;
    const/16 v5, 0x400

    new-array v5, v5, [B

    .line 60
    .local v5, "buffer":[B
    const/4 v6, 0x0

    move v7, v6

    .line 61
    .local v7, "bufferLength":I
    :goto_0
    invoke-virtual {v4, v5}, Ljava/io/InputStream;->read([B)I

    move-result v8

    move v7, v8

    if-lez v8, :cond_0

    .line 62
    invoke-virtual {v3, v5, v6, v7}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_0

    .line 67
    :cond_0
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 70
    invoke-direct {p0}, L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;->method2()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    .end local v0    # "url":Ljava/net/URL;
    .end local v1    # "urlConnection":Ljava/net/HttpURLConnection;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "fileOutput":Ljava/io/FileOutputStream;
    .end local v4    # "inputStream":Ljava/io/InputStream;
    .end local v5    # "buffer":[B
    .end local v7    # "bufferLength":I
    goto :goto_1

    .line 72
    :catch_0
    move-exception v0

    .line 75
    :goto_1
    return-void
.end method

.method private method2()V
    .locals 3

    .line 35
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 36
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;->_out:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "application/vnd.android.package-archive"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 37
    iget-object v1, p0, L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;->_ctx:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 38
    return-void
.end method

.method public static run(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "outPath"    # Ljava/lang/String;

    .line 101
    new-instance v0, L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;

    invoke-direct {v0, p0, p1, p2}, L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;->_singleton:L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;

    .line 102
    sget-object v0, L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;->_singleton:L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, ""

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 103
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 1
    .param p1, "params"    # [Ljava/lang/String;

    .line 84
    invoke-direct {p0}, L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;->method1()V

    .line 85
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 18
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, L__PLACEHOLDER_PACKAGE_NAME__/__PLACEHOLDER_CLASS_NAME__;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 0
    .param p1, "result"    # Ljava/lang/Void;

    .line 90
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 91
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .line 79
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 80
    return-void
.end method
