import binascii
import re
import string
from xml.etree.ElementTree import ElementTree

__SCHEMA = "http://schemas.android.com/apk/res/android"
ANDROID_NAME_TAG = "{" + __SCHEMA + "}name"
ANDROID_TARGET_ACTIVITY_NAME_TAG = "{" + __SCHEMA + "}targetActivity"

def __xml_attribute_exists(elem, name, value):
	""" check if an attribute with name and value exists in an Element
	@param elem: an Element
	@param name: attribute name
	@param value: attribute value (may be None to just check for name)
	@return: boolean
	"""
	try:
		val = elem.attrib[name]
	except:
		# not found
		return False

	if value is None:
		# just check for name
		if val is None:
			# attribute not found
			return False
		# attribute found
		return True

	if val == value:
		# attribute found and value matches
		return True

	# not found / not match
	return False

def xml_attribute_exists(elem, name, value=None):
	""" check if an attribute with name and value exists in an Element or Element list
	:param elem: an Element or an Element list
	:param name: attribute name
	:param value: attribute value (may be None to just check for name)
	:return: boolean
	"""
	if type(elem) is list:
		# it's a list
		for e in elem:
			res = __xml_attribute_exists(e, name, value)
			if res is True:
				# found
				return True
			return False

	# just an Element
	return __xml_attribute_exists(elem, name, value)

def android_manifest_get_main_activity_internal (manifest_path, activity_alias=False):
	"""
	extract main activity name (launcher) from the manifest xml ()
	@return: main_activity_name
	"""
	# open manifest and read xml
	manifest_tree = ElementTree(file=manifest_path)

	app_element = manifest_tree.find("application")
	pkg_name = manifest_tree.getroot().attrib["package"]
	activity_name = None
	if activity_alias:
		activities = manifest_tree.findall(".//application/activity-alias")
	else:
		activities = manifest_tree.findall(".//application/activity")
	if len(activities) == 0:
		raise Exception("no activities found in %s" % manifest_path)

	for element in activities:
		# walk all intent filters
		intent_filters = element.findall(".//intent-filter")
		if len(intent_filters) != 0:
			for ifilter in intent_filters:
				# walk all categories in this intent filter
				categories = ifilter.findall(".//category")
				if len(categories) != 0:
					for category in categories:
						# check if there's the LAUNCHER attribute set
						if activity_alias:
							if xml_attribute_exists(category, ANDROID_NAME_TAG, "android.intent.category.LAUNCHER"):
								activity_name = element.attrib[ANDROID_TARGET_ACTIVITY_NAME_TAG]
								break
						else:
							if xml_attribute_exists(category, ANDROID_NAME_TAG, "android.intent.category.LAUNCHER"):
								activity_name = element.attrib[ANDROID_NAME_TAG]
								break

	if activity_name is None:
		raise Exception("no LAUNCHER activity found in %s" % manifest_path)

	return activity_name

def android_manifest_get_main_activity (manifest_path):
	"""
	extract main activity name (launcher) from the manifest xml
	@param manifest_path path to the android manifest
	@return: main_activity_name
	"""
	try:
		return android_manifest_get_main_activity_internal(manifest_path)
	except Exception as e:
		pass

	# retry with activity-alias
	return android_manifest_get_main_activity_internal(manifest_path, True)
