#!/usr/bin/env python3
"""
skype APK patches
wolf, ht, 2k18
"""

import libutils.customizeutils
import libutils.apkutils
import shutil

# globals
PATH_KEYSTORE = './apps/skype/fake_skype.key'
LAUNCHER_PARENT = 'com.skype4life'
APPNAME = 'Skype'

def prebuild_callback(path_decompiled, args):
	"""
	customized callback to do additional stuff before rebuilding apk (calls the default callback)
	@param path_decompiled path to the decompiled source, default libutils.apkutils.PATH_TMP_FOLDER
	@args arguments for the default callback
	"""

	# copy broken files (this is named .png but is indeed a .jpg, apktool freaks out during rebuild. i just converted it to png to make it happy
	print('[.] replacing wrong jpg -> png to make apktool happy')
	filename = 'rnapp_app_android_images_mslogonotext.png'
	shutil.copyfile('./apps/skype/%s' % filename, '%s/res/drawable-mdpi-v4/%s' % (path_decompiled, filename))

	# call the default callback
	libutils.customizeutils.customize_default_prebuild_callback(path_decompiled, args)

def patch_app(args):
	"""
	patch the application
	"""
	args.path_keystore = PATH_KEYSTORE
	args.launch_activity_parent = LAUNCHER_PARENT
	libutils.customizeutils.customize_default_patch(args, prebuild_callback)

def main():
	libutils.customizeutils.customize_default_main(APPNAME, patch_app)


if __name__ == "__main__":
	main()
